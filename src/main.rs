use std::{env};
use ho_bot::bot::bootstarp::start;

#[tokio::main]
async fn main() {

    let token = env::var("TELEGRAM_BOT_TOKEN").expect("TELEGRAM_BOT_TOKEN not set");
    start(&token).await;
}
