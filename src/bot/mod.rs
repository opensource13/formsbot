pub mod bootstarp;
pub mod state;
pub mod responses;
pub mod form;

use async_trait::async_trait;

use frankenstein::{Message, AsyncApi, SendMessageParams, AsyncTelegramApi, User};
use crate::errors::{BotResult, BotError};


pub async fn validate_user(message: &Message) -> BotResult<()> {
  if let Some(user) = &message.from {
    if user.is_bot {
      return Err(BotError::BadRqeuest(String::from("User is a bot")));
    }
    return Ok(())
  }
  return Err(BotError::BadRqeuest(String::from("Missing user")));
}

#[async_trait]
pub trait BotMessenger {
  fn get_user(&self, message: &Message) -> BotResult<Box<User>>;
  async fn send_text(&self, text: String, message: &Message) ->  BotResult<()>;
}

#[derive(Clone)]
pub struct HoBot {
  pub api: AsyncApi
}

#[async_trait]
impl BotMessenger for HoBot {
  fn get_user(&self, message: &Message) -> BotResult<Box<User>> {
    if let Some(user) = &message.from {
      if user.is_bot {
        return Err(BotError::BadRqeuest(String::from("User is a bot")));
      }
      return Ok(user.clone())
    }
    return Err(BotError::BadRqeuest(String::from("Missing user")));
  }

  async fn send_text(&self, text: String, message: &Message) ->  BotResult<()> {
    let send_message_params = SendMessageParams::builder()
    .chat_id(message.chat.id)
    .text(text)
    .build();

    if let Err(err) = self.api.send_message(&send_message_params).await {
      return Err(BotError::Transmission(format!("{}", err)));
    }
    Ok(())
  }
}