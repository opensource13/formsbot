use super::form::state::{FormState, new_state};

use super::super::db::mongo::Connection;
use super::HoBot;

use crate::modules::users::{
  infra::repository::mongo::MongoUserRepo,
  app::service::UserService
};

/*
* Create services to be used in thee bot process
*/
#[derive(Clone, Debug)]
pub struct Services {
  pub user: UserService
}

impl Services {
  fn new(db: Connection) -> Self {
    let user_repo = MongoUserRepo::new(db);
    let user_svc = UserService::new(user_repo);

    Services { 
      user: user_svc
    }
  }
}

/*
* Create a state to be shared in the bot endpoints
*/
#[derive(Clone)]
pub struct State {
  pub forms: FormState, // Save in memmoty the forms of a bot user
  pub bot: HoBot, // Save a bot client
  pub services: Services // Services for modules
}

impl State {
  pub async fn new(bot: HoBot) -> State {

    let conn = match Connection::new().await {
      Ok(c) => c,
      Err(e) => {
        panic!("{}", e);
      }
    };

    let services = Services::new(conn);
    State {
      forms: new_state(),
      bot,
      services
    }
  }
}