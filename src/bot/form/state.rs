use std::collections::HashMap;
use std::sync::{Arc, Mutex};
use super::engine::Form;



pub type FormState = Arc<Mutex<HashMap<u64, Form>>>;

pub fn new_state() -> FormState {
  return Arc::new(Mutex::new(HashMap::new()));
} 