use std::collections::HashMap;

use crate::bot::state::Services;

pub type FormQuestions = HashMap<String, Question>;
type FinishCallback = fn(form: Form, services: Services);

#[derive(Clone)]
pub enum QuestionType {
  Poll,
  Text
}

/*
* Question of the form
*/

#[derive(Clone)]
pub struct Question {
  pub text: String,
  responses: Vec<String>
}

impl Question {
  pub fn get_responses(&self) -> Vec<String> {
    return self.responses.clone()
  }
}

/*
 * A form represents a series of questions  
*/

#[derive(Clone)]
pub struct Form {
  pub owner_id: u64,
  current: usize,
  fields: Vec<String>,
  data: FormQuestions,
  pub on_finish: Option<FinishCallback>
}


impl Form {
  pub fn new(id: u64) -> Form {
    Form {
      owner_id: id,
      current: 0,
      data: HashMap::new(),
      fields: vec![],
      on_finish: None,
    }
  }

  pub fn get_questions(&self) -> FormQuestions {
    self.data.clone()
  }

  pub fn add_on_finish(&mut self, callback: FinishCallback) {
    self.on_finish = Some(callback);
  }

  pub fn add_question(&mut self, name: String, text: String) {
    self.fields.push(name.clone());

    let q = Question {
      text,
      responses: vec![]
    };
    self.data.insert(name, q);
  }

  pub fn get_question(&self) -> Option<String> {
    if let Some(current_field) = self.fields.get(self.current) {
      if let Some(q) = self.data.get(current_field) {
        return Some(q.text.clone())
      }
      return None
    }
    None
  }


  pub fn add_answers(&mut self, answers: Vec<String>) {
    if let Some(current_field) = self.fields.get(self.current) {
      let question = match self.data.get(current_field) {
        Some(q) => q,
        None => { return () }
      };
      
      let mut q = question.clone();
      q.responses = answers;

      self.data.insert(current_field.clone(), q);
    }
  }
  

  pub fn next(&mut self) { // return true if form is complete
    self.current += 1;
  }
}