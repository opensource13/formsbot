use frankenstein::{ Message };

use crate::errors::{BotResult, BotError};
use crate::bot::{state::State};

use super::BotMessenger;


pub async fn process(message: &Message, state: State) -> BotResult<()> {
  // Get user from message
  let user = state.bot.get_user(message)?;

  let mut forms = state.forms.lock().unwrap();

  // Get form registered for this user
  let user_form = match forms.get(&user.id) {
    Some(u) => u,
    None => { return Err(BotError::BadRqeuest(String::from("User has no open form")))}
  };

  // Get the response message 
  let user_response = match &message.text {
    Some(m) => m,
    None => { return Err(BotError::BadRqeuest(String::from("Missing response for question")))}
  };

  // Save the response
  let mut u_f = user_form.clone();
  let answers = vec![user_response.clone()];
  u_f.add_answers(answers);
  u_f.next();
  forms.insert(user.id, u_f.clone());


 if let Some(q) = u_f.get_question() {
  state.bot.send_text(q, message).await.unwrap();
 } else {
  // Call on finish callback passing data
  if let Some(cb) = u_f.on_finish {
    cb(u_f, state.services.clone());
  }
  forms.remove(&user.id);
  state.bot.send_text("Fin del formulario".to_string(), message).await.unwrap();
 }

  Ok(())
}
