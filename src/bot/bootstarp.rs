/*
* Main entry point for the bot server
* Register all possible actions 
*/

use frankenstein::{AsyncApi, UpdateContent, GetUpdatesParams, AsyncTelegramApi};

use super::{state::State, HoBot, responses::process};

use crate::modules::{
//   assistance::infra::bot::register_month,
  users::infra::bot::register_user,
};


pub async fn start(token: &String) {

  let api = AsyncApi::new(token);
  let messenger = HoBot{api: api.clone()};

  // Create the state to keep forms and answers
  let state = State::new(messenger).await;

  let update_params_builder = GetUpdatesParams::builder();
  let mut update_params = update_params_builder.clone().build();

  loop {
    let result = api.get_updates(&update_params).await;

    match result {
      Ok(response) => {
        for update in response.result {
          if let UpdateContent::Message(message) = update.content {
            if let Some(text) = &message.text {
              match text.as_str() {
                "/start" => {
                  if let Err(e) = register_user(&message, state.clone()).await {
                    println!("{}", e);
                  }
                },
                _ => {
                  if let Err(e) = process(&message, state.clone()).await {
                    println!("{}", e);
                  }
                }
              }
            }

            // Go for the next message
            update_params = update_params_builder
              .clone()
              .offset(update.update_id + 1)
              .build();
        }
      }},
        Err(error) => {
            println!("Failed to get updates: {error:?}");
        }
    }
}
}


  