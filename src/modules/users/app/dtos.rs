#[derive(Debug)]
pub struct CreateUserDto {
  pub bot_id: u64, // user id provided from the bot
  pub sat_id: u64, // user id provided from sat
  pub names: String, // Name registred by the user
  pub last_name: String, // Last name provided by the user
  pub second_last_name: String, // Second last name provided by the user
  pub area_id: u64 // Adscription area of the user
}
