use super::dtos::CreateUserDto;
use crate::errors::{ApplicationError, ApplicationResult};
use super::super::domain::{
  entities::User,
  value_objects::{UserSatId, UserNames, UserLastName, UserSecondLastName, UserAreaId},
};
use super::super::infra::repository::mongo::MongoUserRepo;


// Service with use cases for user
#[derive(Clone, Debug)]
pub struct UserService {
  repository: MongoUserRepo
}

impl UserService {

  pub fn new(repository: MongoUserRepo) -> UserService {
    return UserService { repository };
  }

  // Retrieve by bot id
  pub async fn get_by_bot_id(&self, id: u64) -> ApplicationResult<Option<User>> {
    self.repository.find_by_bot_id(id).await
  }

  // Create a user
  pub async fn create(&self, data: CreateUserDto) -> ApplicationResult<()> {

    if let Some(_) = self.get_by_bot_id(data.bot_id).await? {
      return Err(ApplicationError::RuleError(String::from("User already exists")))
    }

    // Run validations based in value objects
    let sat_id = UserSatId::new(data.sat_id)?;
    let names = UserNames::new(data.names)?;
    let last_name = UserLastName::new(data.last_name)?;
    let second_last_name = UserSecondLastName::new(data.second_last_name)?;
    let area_id = UserAreaId::new(data.area_id)?;

    let new_user = User {
      sat_id: sat_id.value, 
      names: names.value,
      last_name: last_name.value, 
      second_last_name: second_last_name.value, 
      area_id: area_id.value, 
      bot_id: data.bot_id,
      active: true
    };

    self.repository.save(new_user).await
  }
}

