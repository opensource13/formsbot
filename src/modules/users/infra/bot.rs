use frankenstein::{Message, AsyncTelegramApi, SendMessageParams};

use crate::bot::{
  BotMessenger,
  state::State,
};
use crate::errors::{BotResult, BotError};
use super::form::{create_user_form};

pub async fn register_user(message: &Message, state: State) -> BotResult<()> {
  // Get user from message
  let user = state.bot.get_user(message)?;

  let user_svc = state.services.user;
  let mut form = create_user_form(user.id);

  if let Ok(res) = user_svc.get_by_bot_id(user.id).await {
    match res {
      Some(u) => {
        println!("User registred {}", u.bot_id);
        return Ok(())
      },
      None => {
        // Create form for user details
        form = create_user_form(user.id);
        println!("continue");
      }
    }
  }
  
  // Add form to state for future responses
  let mut forms = state.forms.lock().unwrap();
  forms.insert(user.id, form.clone());

  if let Some(q1) = form.get_question() {
    let send_message_params = SendMessageParams::builder()
      .chat_id(message.chat.id)
      .text("Vamos a empezar 😃")
      .reply_to_message_id(message.message_id)
      .build();

    if let Err(err) = state.bot.api.send_message(&send_message_params).await {
      return Err(BotError::Transmission(format!("{}", err)));
    }

    state.bot.send_text(q1, message).await?;  
  } else {
    state.bot.send_text(String::from("Formulario vacío"), message).await?;  
  }  
  
  Ok(())
}