use futures::stream::TryStreamExt;

use mongodb::{Collection, bson::{doc}};

use crate::{db::mongo::{Connection}};
use crate::errors::{ApplicationResult};
use super::super::super::domain::entities::User;

pub static COLLECTION_NAME: &'static str = "users";

#[derive(Clone, Debug)]
pub struct MongoUserRepo {
  collection: Collection<User>
}

impl MongoUserRepo {
  pub fn new(client: Connection) -> MongoUserRepo {
    return MongoUserRepo {
      collection: client.db.collection::<User>(COLLECTION_NAME)
    };
  }


  pub async fn list(&self) -> ApplicationResult<Vec<User>> {
    let mut users: Vec<User> = vec![];
    let mut cursor = self.collection.find(None, None).await?;

    while let Some(user) = cursor.try_next().await? {
      users.push(user);
    };

    Ok(users)
  }

  pub async fn find_by_bot_id(&self, id: u64) -> ApplicationResult<Option<User>> {
    let bot_id = id as u32;
    let user = self.collection.find_one(doc!{"bot_id": bot_id},None).await?;
    Ok(user)
  }
  
  pub async fn save(&self, user: User) -> ApplicationResult<()> {
    self.collection.insert_one(user.clone(), None).await?;
    Ok(())
  }
}
