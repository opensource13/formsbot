use crate::bot::{
  form::engine::Form,
  state::Services,
};
use super::super::app::dtos::CreateUserDto;


pub fn create_user_form(owner_id: u64) -> Form {

  let mut form = Form::new(owner_id);

  form.add_question(String::from("sat_id"), String::from("Ingresa tu número de empleado"));
  form.add_question(String::from("names"), String::from("Ingresa tu(s) nombre(s)"));
  form.add_question(String::from("last_name"), String::from("Ingresa tu Apellido Materno"));
  form.add_question(String::from("second_last_name"), String::from("Ingresa tu Apellido Paterno"));
  form.add_question(String::from("area_id"), String::from("Ingresa tu unidad"));

  fn callback (form: Form, services: Services) {
    let sat_id:u64 = form.get_questions().get("sat_id").unwrap().get_responses().join(",").parse().unwrap();
    let names = form.get_questions().get("names").unwrap().get_responses().join(",");
    let last_name = form.get_questions().get("last_name").unwrap().get_responses().join(",");
    let second_last_name = form.get_questions().get("second_last_name").unwrap().get_responses().join(",");
    let area_id: u64 = form.get_questions().get("area_id").unwrap().get_responses().join(",").parse().unwrap();

  
    let new_user = CreateUserDto {
      bot_id: form.owner_id,
      names,
      last_name,
      second_last_name,
      sat_id,
      area_id
    };

    println!("{:?}", new_user);

    tokio::spawn(async move {
      if let Err(e) = services.user.create(new_user).await {
        println!("{}", e);
      }
    });


  }
  form.add_on_finish(callback);

  return form;
}