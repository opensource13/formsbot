pub struct CreateUser {
  pub bot_id: u64, // user id provided from the bot
  pub names: String, // Name registred by the user
  pub last_name: String, // Last name provided by the user
  pub second_last_name: String, // Second last name provided by the user
  pub area_id: String // Adscription area of the user
}
