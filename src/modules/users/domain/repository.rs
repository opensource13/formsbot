use super::entities::User;
use crate::errors::ApplicationResult;

pub trait UserRepository {  fn save(&self, user: User) -> ApplicationResult<Option<User>>;
  fn find_by_sat_id(&self, id: String) -> ApplicationResult<Option<User>>;
  fn find_by_bot_id(&self, id: String) -> ApplicationResult<Option<User>>;
}