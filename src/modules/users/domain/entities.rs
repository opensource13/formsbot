use serde::{Serialize, Deserialize};


// This is a representation of a db saved user
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct User {
  pub sat_id: u64,
  pub bot_id: u64,
  pub names: String,
  pub last_name: String,
  pub second_last_name: String,
  pub area_id: u64,
  pub active: bool
}
