use validator::Validate;
use crate::errors::ApplicationResult;

// Sat id of the user (187506)
#[derive(Debug, Validate)]
pub struct UserSatId {
    #[validate(range(min=000001,max=999999))]
    pub value: u64
}

impl UserSatId {
    pub fn new(value: u64) -> ApplicationResult<UserSatId> {
        let tmp = UserSatId { value };
        tmp.validate()?;
        Ok(tmp)
    }
}

// Names of a user
#[derive(Debug, Validate)]
pub struct UserNames {
    #[validate(length(min=1))]
    pub value: String
}

impl UserNames {
    pub fn new(names: String) -> ApplicationResult<UserNames> {
        let tmp = UserNames { value: names };
        tmp.validate()?;
        Ok(tmp)
    }
}

// Last name of a user
#[derive(Debug, Validate)]
pub struct UserLastName {
    #[validate(length(min=1))]
    pub value: String
}

impl UserLastName {
    pub fn new(value: String) -> ApplicationResult<UserLastName> {
        let tmp = UserLastName { value };
        tmp.validate()?;
        Ok(tmp)
    }
}

// Second last name
#[derive(Debug, Validate)]
pub struct UserSecondLastName {
    #[validate(length(min=1))]
    pub value: String
}

impl UserSecondLastName {
    pub fn new(value: String) -> ApplicationResult<UserSecondLastName> {
        let tmp = UserSecondLastName { value };
        tmp.validate()?;
        Ok(tmp)
    }
}

// Area id of the user (example 102)
#[derive(Debug, Validate)]
pub struct UserAreaId {
    #[validate(range(min=100,max=999))]
    pub value: u64
}

impl UserAreaId {
    pub fn new(value: u64) -> ApplicationResult<UserAreaId> {
        let tmp = UserAreaId { value };
        tmp.validate()?;
        Ok(tmp)
    }
}
