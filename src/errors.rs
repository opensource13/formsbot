use std::result;
use thiserror::Error;


#[derive(Debug, Error)]
pub enum BotError {
  #[error("Input error in bot process: `{0}`")]
  InputError(String),
  #[error("Transmisson error with Telegram: `{0}`")]
  Transmission(String),
  #[error("Bad request for Ho bot: `{0}`")]
  BadRqeuest(String)
}

pub type BotResult<T> = result::Result<T, BotError>;

#[derive(Debug, Error)]
pub enum ApplicationError {
  #[error("Database error: `{0}`")]
  DBError(mongodb::error::Error),
  #[error("Validation input error: `{0}`")]
  ValidationError(validator::ValidationErrors),
  #[error("Bussines rule error: `{0}`")]
  RuleError(String)
}


impl From<validator::ValidationErrors> for ApplicationError {
  fn from(error: validator::ValidationErrors) -> Self {
    ApplicationError::ValidationError(error)
  } 
}

impl From<mongodb::error::Error> for ApplicationError {
  fn from(error: mongodb::error::Error) -> Self {
    ApplicationError::DBError(error)
  }
}

pub type ApplicationResult<T> = result::Result<T, ApplicationError>;
