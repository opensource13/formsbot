use mongodb::{Client, Database, options::ClientOptions};
use crate::errors::ApplicationResult;

#[derive(Debug, Clone)]
pub struct Connection {
  pub db: Database
}

impl Connection {
  pub async fn new() -> ApplicationResult<Connection> {
    let client_options = ClientOptions::parse("mongodb://root:example@localhost:27017").await?;
    let client = Client::with_options(client_options)?;
    let db = client.database("ho-bot");

    return Ok(Connection{db: db});
  }
}